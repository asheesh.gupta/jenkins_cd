node
{        
  stage('Cloning Repo')
  {
    try
    {
        checkout scm
        readprop = readProperties file: "${workspace}/config.properties"   
        echo "Repo Clonned"
        
    }  
       
    catch(Exception e)
    {
        echo "Clonning Repo Task failed "  
        slackSend (color: '#FFFF00',channel: "${readprop['SLACK_CHANNEL']}", message: "Failed: Job '${env.JOB_NAME}' ${JOB_URL} ") 
        throw e
    }
   
  }

  stage('Stability-Check')
  {
    try
    {   
        sh 'mvn clean compile'
        echo "Compile successful"
    }
    catch(Exception e)
    {
        echo "Validation Check Fails"
        slackSend (color: '#FFFF00',channel:"${readprop['SLACK_CHANNEL']}", message: "Failed: Validation Check ") 

        throw e 
    }
  
  }

  stage('Code-Quality-Check')
  {
    try
    {
        sh 'mvn checkstyle:checkstyle'
        echo "Code-Quality-Checked Successfully"    
           
    }
    
    catch(Exception e)
    { 
        echo "Oops Quality not maintained"
        slackSend (color: '#FFFF00',channel:"${readprop['SLACK_CHANNEL']}", message: "Failed: Quality Check Failed ") 
        throw e
    }
  
  }

  stage('Checkstyle-Report-Publish')
  {
    try
    {
       //checkstyle canRunOnFailed:false, canComputeNew: false, defaultEncoding: '', failedTotalAll: '10', failedTotalHigh: '10', failedTotalLow: '10', failedTotalNormal: '20', healthy: '80', pattern: '**/checkstyle-result.xml', unHealthy: '10', unstableTotalAll: '80', unstableTotalHigh: '80', unstableTotalLow: '80', unstableTotalNormal: '80'
        echo "checkstyle"
    }    
    
    catch(Exception e)
    {
        echo "Reports Are Not Published Kindly check the error"
        slackSend (color: '#FFFF00',channel:"${readprop['SLACK_CHANNEL']}", message: "Failed: Publishing CheckstyleReport ") 
        throw e
    }
    
  }
   stage('Integration-Test')
  {
    try
    {
        sh 'mvn failsafe:integration-test'
        sh 'mvn failsafe:verify'
    }    
    
    catch(Exception e)
    {
        echo "Integration check Kindly fail"
        slackSend (color: '#FFFF00',channel:"${readprop['SLACK_CHANNEL']}", message: "Failed: Integration-Test ") 
        throw e
    }
    
  }

  stage('UNIT-TESTING')
  {
    try    
    {
        sh 'mvn test'    
        echo "JUNIT-TEST-Succesfull"                
    }
    
    catch(Exception e)
    {
        echo "Junit Test Fails"
        slackSend (color: '#FFFF00',channel:"${readprop['SLACK_CHANNEL']}", message: "Failed: Junit-Test Fail ") 
        throw e
    }
  
  }
        
  stage('Publish-UNIT-TEST-Report')
  {
    try
    {
        junit 'target/surefire-reports/TEST*.xml'
        echo "Reports Succesfully Published"               
    }
    
    catch(Exception e)
    {
        echo "Junit Reports Failed to upload"
        slackSend (color: '#FFFF00',channel:"${readprop['SLACK_CHANNEL']}", message: "Failed: Junit Report Not Published") 
        throw e
    }
  
  }

  stage('Code-Coverage-Testing')
  {
    try
    {
        sh 'mvn cobertura:cobertura'
    }
    
    catch(Exception e)
    {
        echo "Kindly check your pom file or spelling for cobertura"
        slackSend (color: '#FFFF00',channel:"${readprop['SLACK_CHANNEL']}", message: "Failed: Code-Coverage Stage Failed ") 
        throw e
    }
  
  }

  stage('Cobertura-Report-Publish')
  {
    try
    {
        cobertura(
        autoUpdateHealth: false, 
        autoUpdateStability: false, 
        coberturaReportFile: '**/target/site/cobertura/coverage.xml',
        failUnhealthy: true, 
        failUnstable: true, 
        conditionalCoverageTargets: '70, 0, 0', 
        fileCoverageTargets: '70, 0, 0', 
        lineCoverageTargets: '80, 0, 0', 
        maxNumberOfBuilds: 0, 
        methodCoverageTargets: '80, 0, 0', 
        packageCoverageTargets: '80, 0, 0', 
        sourceEncoding: 'ASCII', 
        zoomCoverageChart: false
        )
    }
    
    catch(Exception e)
    {
        echo "Failed to upload Cobertura reports"
        slackSend (color: '#FFFF00',channel: "${readprop['SLACK_CHANNEL']}", message: "Failed: Cobertura Reports not Published ") 
        throw e
    }
  
  }
    
  stage('Package')
  {
        sh ' mvn package'
  }
  
  stage('SonarQube analysis')
  {
    try 
    {   
        withSonarQubeEnv('sonar_server')
        {        
            def scannerHome = tool 'sonar_scanner';   
            sh "${tool("sonar_scanner")}/bin/sonar-scanner"
            echo "Data in Sonar-Qube Successfully Pushed"    
            echo "${readprop.SLACK_CHANNEL}"
            slackSend channel:"${readprop['SLACK_CHANNEL']}", message: 'SonarQube data published'
        }            
    }
    
    catch(Exception e)
    {
        echo "Sonarqube data not pushed"
        slackSend (color: '#FFFF00',channel:"${readprop['SLACK_CHANNEL']}", message: "Failed: SonarQube Data not published ") 
        throw e
            
    }
  
  }
  
  /* stage("Quality Gate")
    {
    timeout(time: 10, unit: 'SECONDS')
    {
    // Just in case something goes wrong, pipeline will be killed after a timeout
    def qg = waitForQualityGate() 
    // Reuse taskId previously collected by withSonarQubeEnv
    if (qg.status != 'OK')
    {
      error "Pipeline aborted due to quality gate failure: ${qg.status}"
    }
    }
    }
    */
    
    
  stage('Deploying-Artifactory-To-Nexus')
  { 
    try   
    {  
        sh "ls target/"
        nexusPublisher( 
        nexusInstanceId: "${readprop['NEXUS_INSTANCE_ID']}", 
        nexusRepositoryId: "${readprop['NEXUS_REPOSITORY_IP']}", 
        packages: [[$class: 'MavenPackage', 
        mavenAssetList: [[classifier: '', 
        filePath: "${workspace}/target/payslip-1.war"]], 
        mavenCoordinate: [artifactId:"${readprop['ARTIFACTID']}", 
        groupId: "${readprop['GROUPID']}", 
        packaging: 'war', 
        version:"${readprop['VERSIONID']}"]]]
        )
                    
    }
      
    catch(Exception e)
    {
        echo "Deploying artifacts to nexus failed"
        slackSend (color: '#FFFF00',channel: "${readprop.SLACK_CHANNEL}" , message: "Failed: Artifact Deployment too nexus failed ") 
        throw e
    }
  
  }    

}

